	<?php
	error_reporting(-1);
	ini_set("display_errors",1);

	$cwd = getcwd(); //
	$upload_dir_full_path = ''; // Initializing variable to avoid a notice. 

	$delete_whitelist = [$php_script,$javascript,$css,'.htaccess','php.ini','error_log'];
			
	$myvar = strpos($cwd,'public_html');
	// echo substr($cwd,0,$myvar)."g-up/";
	if ($myvar){
		
		$upload_dir_full_path = substr($cwd,0,$myvar).$upload_dir_name;
		
		if (!file_exists($upload_dir_full_path)){
			mkdir($upload_dir_full_path,0755);
		}


	}else{
		$upload_dir_full_path = $cwd.'/../../';
	}


	// echo "<br>Destination: $upload_dir_full_path";

	$upload_info = new stdClass;
	$auth_status = new stdClass;

	function incrementFileName($filename){
		$file_exploded = explode(".",$filename);
		$file_ext = '.'.end($file_exploded);
		$root_name = str_replace(($file_ext),"",$filename);
		$file = $filename;
		$i = 1;
		while(file_exists($file)){
			$file = $root_name." ($i)".$file_ext;
			$i++;
		}
		return $file;
		// echo $file;
	}

	//self delete
	function seppuku($working_dir,$delete_whitelist){

		//print_r($delete_whitelist);

		$handle = opendir('.');

		for ($i = 0; $i < count($delete_whitelist); $i++){
			if (file_exists($delete_whitelist[$i])){
				// echo "removing $delete_whitelist[$i]<br>";
				unlink($delete_whitelist[$i]);	
			}
		}
		
		rmdir('../'.$working_dir);
	}

	function goBabyGo($upload_info, $upload_dir_full_path, $working_dir, $delete_whitelist){
		

		for ($i = 0; $i <count($_FILES['uploadedfile']['name']); $i++) {
			$file_name = basename($_FILES['uploadedfile']['name'][$i]);
			// echo "base:".$file_name."<br>"; //testing
			//$target_file = '../'.$upload_dir_full_path.$file_name; 

			$target_file = $upload_dir_full_path.$file_name;

			if (file_exists($target_file)){
				//echo "{file_exists:1,message:'$target_file exists, incrementing file name'}";
				$upload_info -> file_already_exists = 'true';			
				$target_file = incrementFileName($target_file);
			}else{
				$upload_info -> file_already_exists = 'false';
			}

			$upload_info -> file_name = $target_file;

			// echo "target file: ".$target_file;
			if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'][$i], $target_file)) {
			    
			    //echo "{error:0,message:'$target_file uploaded'}";
			    $upload_info -> upload_success = 'true';

			} else{
			    // echo "{error:1,message:'Error uploading ".$_FILES['uploadedfile']['name'][$i]."'}";
			    $upload_info -> upload_success = 'false';
			    // echo "Error uploading $_FILES['uploadedfile']['name'][$i]";
			}
		}

		$json_upload_info = json_encode($upload_info);
		echo $json_upload_info;
		seppuku($working_dir,$delete_whitelist);
	}

///////////////////

	if (isset($_GET['secret'])){

		if (isset($_GET['secret']) && $_GET['secret'] == $secret ){

			goBabyGo($upload_info,$upload_dir_full_path, $working_dir, $delete_whitelist);

		}else{
			$auth_status -> auth_status = '0';
			$auth_status = json_encode($auth_status);
			echo $auth_status;
			// echo "{password_ok:0,message:'Password no workie'}";
			// seppuku();
		}
	}elseif (isset($_POST['seppuku'])){
		seppuku($working_dir,$delete_whitelist);
	}else{
		?>
			<script src="javascript.js"></script> 
			<form enctype="multipart/form-data" action="<?php echo $php_script; ?>" method="POST">
			<input type="hidden" name="guerilla_uploader" />
			Choose a file to upload: <input name="uploadedfile[]" type="file" multiple='multiple' /><br />
			<!-- <input type="text" placeholder="password" name="password" value="" /> -->
			<input type="submit" value="Upload File" />

			</form>
		<?php
	}

	?>